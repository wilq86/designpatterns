﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DesignPatterns.Structural.Adapters
{
    public interface ITarget
    {
        string TargetMethod();
    }

    public class Adapter : ITarget
    {
        private Adaptee Adaptee { get; }

        public Adapter(Adaptee adaptee)
        {
            Adaptee = adaptee;
        }

        public string TargetMethod()
        {
            return Adaptee.MethodToAdaptee();
        }
    }

    public class Adaptee
    {
        public string MethodToAdaptee()
        {
            return "MethodToAdaptee";
        }
    }
}
