﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DesignPatterns.Structural.Composites
{
    public abstract class Component
    {
        protected List<Component> Childrens = new List<Component>();

        public abstract double Cost { get; }

        public void Add(Component children)
        {
            Childrens.Add(children);
        }

        public void Remove(Component children)
        {
            Childrens.Remove(children);
        }

        public double CalculateCost()
        {
            double cost = Cost;
            foreach(var children in Childrens)
            {
                cost += children.Cost;
            }
            return cost;
        }
    }

    public class Box : Component
    {
        public override double Cost
        {
            get { return 1; }
        }
    }

    public class BigBox : Component
    {
        public override double Cost
        {
            get { return 10; }
        }
    }

    public class ComputerCase : Component
    {
        public override double Cost
        {
            get { return 100; }
        }

        public ComputerCase()
        {
            Add(new CPU());
            Add(new GPU());
        }

    }

    internal class GPU : Component
    {
        public override double Cost
        {
            get { return 500; }
        }
    }

    internal class CPU : Component
    {
        public override double Cost
        {
            get { return 200; }
        }
    }
}
