﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DesignPatterns.Structural.Proxies
{
    public interface ISendEmail
    {
        void Send();
    }

    public class SendEmail : ISendEmail
    {
        public void Send()
        {

        }
    }

    public class Proxy : ISendEmail
    {
        private ISendEmail SendEmail;

        public Proxy(ISendEmail multipier)
        {
            SendEmail = multipier;
        }

        public void Send()
        {
            CheckAccess();
            SendEmail.Send();
        }

        public bool CheckAccess()
        {
            throw new Exception("Not authorized");
        }
    }
}
