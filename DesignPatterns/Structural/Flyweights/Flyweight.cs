﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DesignPatterns.Structural.Flyweights
{
    public class Font
    {
        public int Size { get; }

        public bool Bold { get; }

        public string Name { get; }

        public Font(int size, bool bold, string name)
        {
            Size = size;
            Bold = bold;
            Name = name;
        }

        public override bool Equals(object obj)
        {
            return obj is Font font &&
                   Size == font.Size &&
                   Bold == font.Bold &&
                   Name == font.Name;
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(Size, Bold, Name);
        }
    }

    public class FontFactory
    {
        private HashSet<Font> Fonts = new HashSet<Font>();

        public Font CreateFont(int size, bool bold, string name)
        {
            Font font = new Font(size, bold, name);
            if (Fonts.TryGetValue(font, out Font outFont))
            {
                return outFont;
            }
            else
            {
                Fonts.Add(font);
                return font;
            }
        }
    }
}
