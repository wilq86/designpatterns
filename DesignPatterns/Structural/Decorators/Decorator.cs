﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Text;

namespace DesignPatterns.Structural.Decorators
{
    public interface IFileOperation
    {

        void Save(string path, byte[] data);

        byte[] Read(string path);
    }

    public class FileOperation : IFileOperation
    {
        public virtual byte[] Read(string path)
        {
            return File.ReadAllBytes(path);
        }

        public virtual void Save(string path, byte[] data)
        {
            File.WriteAllBytes(path, data);
        }
    }

    public class CompressFileOperation : FileOperation
    {
        public override void Save(string path, byte[] data)
        {
            base.Save(path, CompressionHelper.Zip(data));
        }

        public override byte[] Read(string path)
        {
            return CompressionHelper.Unzip(base.Read(path));
        }
    }

    public class EncryptFileOperation : CompressFileOperation
    {
        public override byte[] Read(string path)
        {
            return EncryptionHelper.Decrypt(base.Read(path));
        }

        public override void Save(string path, byte[] data)
        {
            base.Save(path, EncryptionHelper.Encrypt(data));
        }
    }

    public class CompressionHelper
    {
        public static byte[] Zip(byte[] bytes)
        {
            using (var msi = new MemoryStream(bytes))
            using (var mso = new MemoryStream())
            {
                using (var gs = new GZipStream(mso, CompressionMode.Compress))
                {
                    CopyTo(msi, gs);
                }

                return mso.ToArray();
            }
        }

        public static byte[] Unzip(byte[] bytes)
        {
            using (var msi = new MemoryStream(bytes))
            using (var mso = new MemoryStream())
            {
                using (var gs = new GZipStream(msi, CompressionMode.Decompress))
                {
                    CopyTo(gs, mso);
                }

                return mso.ToArray();
            }
        }

        public static void CopyTo(Stream src, Stream dest)
        {
            byte[] bytes = new byte[4096];

            int cnt;

            while ((cnt = src.Read(bytes, 0, bytes.Length)) != 0)
            {
                dest.Write(bytes, 0, cnt);
            }
        }
    }

    public class EncryptionHelper
    {
        private static byte key = 0xAA;

        public static byte[] Encrypt(byte[] data)
        {
            return data.Select(x => (byte)(x ^ key)).ToArray();

        }

        public static byte[] Decrypt(byte[] data)
        {
            return data.Select(x => (byte)(key ^ x)).ToArray();
        }
    }

    
}
