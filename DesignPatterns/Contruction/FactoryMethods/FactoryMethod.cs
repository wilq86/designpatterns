﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DesignPatterns.Contruction.FactoryMethods
{
    public interface IProduct
    {
        string Name { get; }
    }

    public interface IFactory
    {
        IProduct CreateProduct();
    }

    class ProductA : IProduct
    {
        public string Name => "A";
    }
    class ProductB : IProduct
    {
        public string Name => "B";
    }
    
    class ProductC : IProduct
    {
        public string Name => "C";
    }

    class Factory : IFactory
    {
        public IProduct CreateProduct()
        {
            if ((int)DateTime.Now.DayOfWeek % 3 == 0)
            {
                return new ProductA();
            }
            else if ((int)DateTime.Now.DayOfWeek % 3 == 1)
            {
                return new ProductB();
            }
            else
            {
                return new ProductC();
            }
        }
    }
}
