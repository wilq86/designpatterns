﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DesignPatterns.Contruction.AbstractFactory
{
    public interface ILogger
    {
        void Info(string message);
    }

    public interface ILoggerFactory
    {
        ILogger GetLogger();
    }

    public class FileLogger : ILogger
    {
        private string FilePath;

        public FileLogger(string filePath)
        {
            FilePath = filePath;
        }

        public void Info(string message)
        {
            
        }
    }

    public class ConsoleLogger : ILogger
    {
        public void Info(string message)
        {
            Console.WriteLine(message);
        }
    }

    public class LoggerFactory : ILoggerFactory
    {
        public bool FileConfiguration;

        public LoggerFactory(bool fileConfiguration, string path)
        {
            FileConfiguration = fileConfiguration;
        }

        public ILogger GetLogger()
        {
            if (FileConfiguration)
            {
                return new FileLogger("path");
            }
            else
            {
                return new ConsoleLogger();
            }
        }
    }

}
