﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DesignPatterns.Contruction.Prototypes
{
    class Prototype : ICloneable
    {
        public object Clone()
        {
            return new Prototype();
        }
    }
}
