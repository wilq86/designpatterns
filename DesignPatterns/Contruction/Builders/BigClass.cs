﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DesignPatterns.Contruction.Builder
{
    /// <summary>
    /// Przykład builder-a który potrafi stworzyć obiekt typu immutable bez konieczności używania konstruktora z 6 argumentami
    /// </summary>
    public class BigClass
    {
        public string ProperthisA { get; private set; }
        public string ProperthisB { get; private set; }
        public string ProperthisC { get; private set; }
        public string ProperthisD { get; private set; }
        public string ProperthisE { get; private set; }
        public string ProperthisF { get; private set; }

        public class Builder
        {
            public string ProperthisA { get; private set; }
            public string ProperthisB { get; private set; }
            public string ProperthisC { get; private set; }
            public string ProperthisD { get; private set; }
            public string ProperthisE { get; private set; }
            public string ProperthisF { get; private set; }

            public Builder()
            {

            }

            public Builder WithProperthisA(string value)
            {
                this.ProperthisA = value;
                return this;
            }

            public Builder WithProperthisB(string value)
            {
                this.ProperthisB = value;
                return this;
            }

            public Builder WithProperthisC(string value)
            {
                this.ProperthisC = value;
                return this;
            }

            public Builder WithProperthisD(string value)
            {
                this.ProperthisD = value;
                return this;
            }

            public Builder WithProperthisE(string value)
            {
                this.ProperthisE = value;
                return this;
            }

            public Builder WithProperthisF(string value)
            {
                this.ProperthisF = value;
                return this;
            }

            public BigClass Build()
            {
                return new BigClass()
                {
                    ProperthisA = ProperthisA,
                    ProperthisB = ProperthisB,
                    ProperthisC = ProperthisC,
                    ProperthisD = ProperthisD,
                    ProperthisE = ProperthisE,
                    ProperthisF = ProperthisF
                };
            }
        }

        private BigClass()
        {

        }
    }
}
