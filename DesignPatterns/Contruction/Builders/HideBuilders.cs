﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace DesignPatterns.Contruction.Builder
{
    public class LogInfo
    {
        public DateTime Timestamp { get; private set; }
        public string Message { get; private set; }

        public LogInfo(string message)
        {
            Message = message;
        }

        public override string ToString()
        {
            return $"{Timestamp:yyyy-MM-dd HH:mm:ss} | {Message}";
        }
    }

    public interface ITarget
    {
        void Write(LogInfo log);
    }

    public class FileTarget : ITarget
    {
        public string FileName { get; private set; }

        public FileTarget(string fileName)
        {
            FileName = fileName;
        }

        public void Write(LogInfo log)
        {
            File.AppendAllText(FileName, log.ToString());
        }
    }

    public class ConsoleTarget : ITarget
    {
        public void Write(LogInfo log)
        {
            Console.WriteLine(log.ToString());
        }
    }

    public interface ITargetBuilder
    {
        ITarget Create();
    }

    public class LogManager
    {
        private ITargetBuilder TargetBuilder;

        public LogManager()
        {
            TargetBuilder = new ConsoleTargetBuilder();
        }

        public ITargetBuilder GetTargetBuilder()
        {
            return TargetBuilder;
        }
    }

    public class FileTargetBuilder : ITargetBuilder
    {
        private string FilePath;
        public FileTargetBuilder(string filePath)
        {
            FilePath = filePath;
        }

        public ITarget Create()
        {
            return new FileTarget(FilePath);
        }
    }

    public class ConsoleTargetBuilder : ITargetBuilder
    {
        public ITarget Create()
        {
            return new ConsoleTarget();
        }
    }
}
