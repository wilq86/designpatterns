﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DesignPatterns.Contruction.Singletons
{
    public class Singleton
    {
        private static object sync = new object();

        private static Singleton _Instance;

        public static Singleton Instance
        {
            get
            {
                lock (sync)
                {
                    if (_Instance == null)
                    {
                        _Instance = new Singleton();
                    }
                    return _Instance;
                }
            }
        }
    }
}
