﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DesignPatterns.Contruction.Singletons
{
    /// <summary>
    /// https://csharpindepth.com/articles/singleton
    /// </summary>
    public class SingletonWithLazy
    {
        public static Lazy<SingletonWithLazy> Instance = new Lazy<SingletonWithLazy>(() => new SingletonWithLazy(), true);

        private SingletonWithLazy()
        {
        }
    }
}
