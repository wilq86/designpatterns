﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace DesignPatterns.Function.TemplateMethods
{
    public abstract class AbstractFormula
    {
        protected List<double> Values = new List<double>();

        public AbstractFormula(List<double> values)
        {
            Values = values;
        }

        protected abstract void Init();

        protected abstract bool Validate();

        protected abstract double Calculate();

        public double? Run()
        {
            Init();

            if (Validate())
            {
                return Calculate();
            }
            else
            {
                return null;
            }
        }
    }

    public class SumPositiveNumbersFormula : AbstractFormula
    {
        public SumPositiveNumbersFormula(List<double> values)
            : base(values)
        {

        }

        protected override void Init()
        {
            
        }

        protected override double Calculate()
        {
            return Values.Sum(x => x);
            
        }

        protected override bool Validate()
        {
            return Values.All(x => x > 0);
        }
    }

    public class MaxFormula : AbstractFormula
    {
        public MaxFormula(List<double> values) 
            : base(values)
        {

        }

        protected override void Init()
        {

        }

        protected override double Calculate()
        {
            return Values.Max(x => x);

        }

        protected override bool Validate()
        {
            return Values.Count > 0;
        }
    }
}
