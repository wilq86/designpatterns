﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace DesignPatterns.Function.Iterator
{
    public class FibonaciIterator : IEnumerable<double>
    {
        public IEnumerator<double> GetEnumerator()
        {
            return new FibonaciEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return new FibonaciEnumerator();
        }
    }

    public class FibonaciEnumerator : IEnumerator<double>
    {
        public int N { get; private set; }

        private double Previous { get; set; }

        public double Current { get; private set; }

        object IEnumerator.Current { get { return Current; }  }

        public void Dispose()
        {
            
        }

        public bool MoveNext()
        {
            if (N == 0)
            {
                Current = 0;
            }
            else if (N == 1)
            {
                Previous = 0;
                Current = 1;
            }
            else
            {
                double temp = Current;
                Current = Current + Previous;
                Previous = temp;
            }
            N++;
            return true;
        }

        public void Reset()
        {
            N = 0;
            Previous = 0;
            Current = 0;
        }
    }
}
