﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DesignPatterns.Function.States
{
    public class StateMachine
    {
        private AbstractState State;

        public StateMachine(AbstractState state)
        {
            State = state;
            State.StateMachine = this;
        }

        public void ChangeState(AbstractState state)
        {
            State = state;
            State.StateMachine = this;
        }

        public string Process()
        {
            return State.Render();
        }
    }

    public abstract class AbstractState 
    {
        public StateMachine StateMachine { get; set; }

        public abstract string Render();
    }

    public class StateInitial : AbstractState
    {
        public override string Render()
        {
            StateMachine.ChangeState(new StateVerifired());
            return "Initial";
        }
    }

    public class StateVerifired : AbstractState
    {
        public override string Render()
        {
            StateMachine.ChangeState(new StateAccepted());
            return "Verified";
        }
    }

    public class StateAccepted : AbstractState
    {
        public override string Render()
        {
            return "Accepted";
        }
    }

}
