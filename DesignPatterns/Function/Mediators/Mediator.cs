﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DesignPatterns.Function.Mediators
{
    public interface IMediator
    {
        void Notify(object sender, string message);
    }

    public class Mediator : IMediator
    {
        public List<string> Messages = new List<string>();

        public void Notify(object sender, string message)
        {
            Messages.Add(message);
        }
    }

    public class Scheduler
    {
        private IMediator Mediator;

        public Scheduler(IMediator mediator)
        {
            Mediator = mediator;
        }

        public void RunScheduler()
        {
            Mediator.Notify(this, "RunScheduler");
        }
    }

    public class Importer 
    {
        private IMediator Mediator;

        public Importer(IMediator mediator)
        {
            Mediator = mediator;
        }

        public void RunImport()
        {
            Mediator.Notify(this, "RunImport");
        }

        public void RunFileImport()
        {
            Mediator.Notify(this, "RunFileImport");
        }
    }
}
