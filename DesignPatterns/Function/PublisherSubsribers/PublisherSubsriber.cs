﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DesignPatterns.Function.PublisherSubsribers
{
    public class Message
    {
        public string Topic { get; }
        public string Text { get; }

        public Message(string topic, string text)
        {
            Topic = topic;
            Text = text;
        }
    }
         
    public interface ISubscriber
    {
        void NewMessage(Message message);
    }

    public interface IPublisher
    {
        void Subscribe(string topic, ISubscriber subscriber);

        void Unsubscribe(string topic, ISubscriber subscriber);
    }


    public class Publisher : IPublisher
    {
        private Dictionary<string, List<ISubscriber>> Subscribers = new Dictionary<string, List<ISubscriber>>();

        public void Publish(Message message)
        {
            if(!Subscribers.ContainsKey(message.Topic))
            {
                return;
            }

            foreach(var subscriber in Subscribers[message.Topic])
            {
                subscriber.NewMessage(message);
            }
        }

        public void Subscribe(string topic, ISubscriber subscriber)
        {
            if (!Subscribers.ContainsKey(topic))
            {
                Subscribers.Add(topic, new List<ISubscriber>());
            }

            Subscribers[topic].Add(subscriber);
        }

        public void Unsubscribe(string topic, ISubscriber subscriber)
        {
            if (!Subscribers.ContainsKey(topic))
            {
                return;
            }

            Subscribers[topic].Remove(subscriber);

            if (Subscribers[topic].Any())
            {
                Subscribers.Remove(topic);
            }
        }
    }

    public class SubscriberWithHistory : ISubscriber
    {
        public List<Message> RecivedMessages = new List<Message>();
        public void NewMessage(Message message)
        {
            RecivedMessages.Add(message);
        }
    }
}
