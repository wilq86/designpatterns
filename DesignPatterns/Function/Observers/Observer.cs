﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DesignPatterns.Function.Observers
{
    public class NotifyEventArgs : EventArgs
    {
        public string Message { get; }

        public NotifyEventArgs(string message)
        {
            Message = message;
        }
    }

    public interface INotify
    {
        event EventHandler<NotifyEventArgs> Notify;
    }

    public class Observer
    {
        public List<NotifyEventArgs> Args = new List<NotifyEventArgs>();

        public void Add(INotify watched)
        {
            watched.Notify += Watched_OnWatch;
        }

        public void Remove(INotify watched)
        {
            watched.Notify -= Watched_OnWatch;
        }

        private void Watched_OnWatch(object sender, NotifyEventArgs args)
        {
            Args.Add(args);
        }
    }

    public class Job : INotify
    {
        public Job()
        {

        }

        public event EventHandler<NotifyEventArgs> Notify;

        public void Run(int count)
        {
            for (int i = 0; i < count; i++)
            {
                Notify?.Invoke(this, new NotifyEventArgs(i.ToString()));
            }
        }
    }
}
