﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DesignPatterns.Function.Strategies
{
    public interface IPrimalityTestStrategy
    {
        bool Check(int value);
    }

    public class SimpleStrategy : IPrimalityTestStrategy
    {
        public bool Check(int value)
        {
            for (int i = 2; i < value / 2; i++)
            {
                if (value % i == 0)
                {
                    return false;
                }
            }

            return true;
        }
    }

    public class FermatStrategy : IPrimalityTestStrategy
    {
        public int Attempts { get; }

        public FermatStrategy(int attempts)
        {
            Attempts = attempts;
        }

        public bool Check(int value)
        {
            int a, i;
            Random rand = new Random();

            if (value < 4)
            {
                return true;
            }

            for (i = 0; i < Attempts; i++)
            {
                a = (int)rand.Next(value - 2) + 2;
                if (PowerModuloFast(a, value - 1, value) != 1)
                {
                    return false;
                }
            }

            return true;
        }

        private static int PowerModuloFast(int a, int b, int m)
        {
            int i;
            long result = 1;
            long x = a % m;

            for (i = 1; i <= b; i <<= 1)
            {
                x %= m;
                if ((b & i) != 0)
                {
                    result *= x;
                    result %= m;
                }
                x *= x;
            }

            return (int)result % m;
        }
    }

    public class PrimeResolver : IPrimalityTestStrategy
    {
        private IPrimalityTestStrategy Strategy;

        public PrimeResolver(IPrimalityTestStrategy strategy)
        {
            Strategy = strategy;
        }

        public bool Check(int value)
        {
            return Strategy.Check(value);
        }
    }
}
