﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DesignPatterns.Function.ChainOfResponsibilities
{
    public interface IHandler
    {
        IHandler Next { get; set; }
        string HandleRequest(object Request);
    }

    public class IntHandler : IHandler
    {
        public IHandler Next { get; set; }

        public string HandleRequest(object request)
        {
            if (request is int value)
            {
                return value.ToString();
            }
            else
            {
                return Next?.HandleRequest(request);
            }
        }
    }
    public class DoubleHandler : IHandler
    {
        public IHandler Next { get; set; }

        public string HandleRequest(object request)
        {
            if(request is double value)
            {
                return value.ToString();
            }
            else
            {
                return Next?.HandleRequest(request);
            }
        }
    }
}
