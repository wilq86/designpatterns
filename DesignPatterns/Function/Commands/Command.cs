﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DesignPatterns.Function.Commands
{
    public class ValueItem
    {
        public double Value { get; set; }

        public ValueItem(double value)
        {
            Value = value;
        }
    }

    public abstract class Command
    {
        public abstract ValueItem Do(ValueItem item);
        public abstract ValueItem Undo(ValueItem item);
    }

    public class MultipleCommand : Command
    {
        public double Multiplier { get; protected set; }

        public MultipleCommand(double multiplier)
        {
            Multiplier = multiplier;
        }

        public override ValueItem Do(ValueItem item)
        {
            item.Value *= Multiplier;
            return item;
        }

        public override ValueItem Undo(ValueItem item)
        {
            item.Value /= Multiplier;
            return item;
        }
    }

    public class AddCommand : Command
    {
        public double Add { get; protected set; }

        public AddCommand(double add)
        {
            Add = add;
        }

        public override ValueItem Do(ValueItem item)
        {
            item.Value += Add;
            return item;
        }

        public override ValueItem Undo(ValueItem item)
        {
            item.Value -= Add;
            return item;
        }
    }
}
