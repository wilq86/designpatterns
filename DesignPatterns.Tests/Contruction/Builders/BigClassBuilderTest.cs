﻿using DesignPatterns.Contruction.Builder;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace DesignPatterns.Tests.Contruction.Builder
{
    [TestFixture]
    public class BigClassBuilderTest
    {
        [Test]
        public void CreatingWithBuilder()
        {
            //Compile error because contructor of BigClass is private, you have to use Builder to create this object 
            //BigClass big = new BigClass();

            BigClass.Builder builder = new BigClass.Builder();

            builder.WithProperthisA("A")
                .WithProperthisB("B")
                .WithProperthisC("C")
                .WithProperthisD("D")
                .WithProperthisE("E")
                .WithProperthisF("F");

            BigClass bigClass = builder.Build();

            Assert.AreEqual(bigClass.ProperthisA, "A");
            Assert.AreEqual(bigClass.ProperthisB, "B");
            Assert.AreEqual(bigClass.ProperthisC, "C");
            Assert.AreEqual(bigClass.ProperthisD, "D");
            Assert.AreEqual(bigClass.ProperthisE, "E");
            Assert.AreEqual(bigClass.ProperthisF, "F");
        }
    }
}
