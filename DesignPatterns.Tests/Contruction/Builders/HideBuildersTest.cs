﻿using DesignPatterns.Contruction.Builder;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace DesignPatterns.Tests.Contruction.Builder
{
    [TestFixture]
    public class HideBuildersTest
    {
        [Test]
        public void CreateTarget()
        {
            LogManager logManager = new LogManager();
            ITarget target = logManager.GetTargetBuilder().Create();

            target.Write(new LogInfo("Test"));
        }
    }
}
