﻿using DesignPatterns.Contruction.Singletons;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace DesignPatterns.Tests.Contruction.Singletons
{
    [TestFixture]
    public class SingletonWithLazyTest
    {
        [Test]
        public void shouldCreateOnlyOnceInstance()
        {
            Assert.IsFalse(SingletonWithLazy.Instance.IsValueCreated);
            Assert.IsTrue(SingletonWithLazy.Instance.Value != null);
            Assert.IsTrue(SingletonWithLazy.Instance.IsValueCreated);
        }
    }
}
