﻿using DesignPatterns.Function.States;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace DesignPatterns.Tests.Function.States
{
    [TestFixture]
    public class State
    {
        [Test]
        public void shouldTestStates()
        {
            StateMachine stateMachine = new StateMachine(new StateInitial());

            Assert.AreEqual(stateMachine.Process(), "Initial");
            Assert.AreEqual(stateMachine.Process(), "Verified");
            Assert.AreEqual(stateMachine.Process(), "Accepted");

        }
    }
}
