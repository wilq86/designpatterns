﻿using DesignPatterns.Function.Strategies;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace DesignPatterns.Tests.Function.Strategies
{
    [TestFixture]
    public class StrategyTest
    {
        [Test]
        public  void shouldTestStrategy()
        {
            PrimeResolver resolver = new PrimeResolver(new SimpleStrategy());

            Assert.AreEqual(resolver.Check(51), false);
            Assert.AreEqual(resolver.Check(52), false);
            Assert.AreEqual(resolver.Check(43), true);
            Assert.AreEqual(resolver.Check(97), true);

            resolver = new PrimeResolver(new FermatStrategy(100));

            Assert.AreEqual(resolver.Check(200), false);
            Assert.AreEqual(resolver.Check(271), true);
        }
    }
}
