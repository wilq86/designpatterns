﻿using DesignPatterns.Function.Mediators;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace DesignPatterns.Tests.Function.Mediators
{
    [TestFixture]
    public class MediatorTest
    {
        [Test]
        public void shouldTestMediator()
        {
            Mediator mediator = new Mediator();

            Scheduler scheduler= new Scheduler(mediator);
            Importer importer= new Importer(mediator);

            scheduler.RunScheduler();
            importer.RunFileImport();
            scheduler.RunScheduler();
            importer.RunImport();
            importer.RunFileImport();
            scheduler.RunScheduler();

            Assert.AreEqual(mediator.Messages[0], "RunScheduler");
            Assert.AreEqual(mediator.Messages[1], "RunFileImport");
            Assert.AreEqual(mediator.Messages[2], "RunScheduler");
            Assert.AreEqual(mediator.Messages[3], "RunImport");
            Assert.AreEqual(mediator.Messages[4], "RunFileImport");
            Assert.AreEqual(mediator.Messages[5], "RunScheduler");
        }
    }
}
