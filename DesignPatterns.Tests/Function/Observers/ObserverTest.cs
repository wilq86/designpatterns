﻿using DesignPatterns.Function.Observers;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace DesignPatterns.Tests.Function.Observers
{
    [TestFixture]
    public class ObserverTest
    {
        [Test]
        public void shouldCheckObserver()
        {
            Observer observer = new Observer();
            Job job = new Job();

            observer.Add(job);

            job.Run(10);
            Assert.AreEqual(observer.Args.Count, 10);

            job.Run(20);
            Assert.AreEqual(observer.Args.Count, 30);

            observer.Remove(job);
            
            job.Run(20);
            Assert.AreEqual(observer.Args.Count, 30);
        }

    }
}
