﻿using DesignPatterns.Function.PublisherSubsribers;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace DesignPatterns.Tests.Function.PublisherSubsribers
{
    [TestFixture]
    public class PublisherSubsriberTest
    {
        [Test]
        public void shouldTestSubsribing()
        {
            Publisher publisher = new Publisher();
            SubscriberWithHistory subscriberA = new SubscriberWithHistory();
            SubscriberWithHistory subscriberB = new SubscriberWithHistory();

            publisher.Subscribe("A", subscriberA);
            publisher.Subscribe("B", subscriberB);

            publisher.Publish(new Message("A", "1"));
            publisher.Publish(new Message("B", "2"));
            publisher.Publish(new Message("B", "3"));

            Assert.AreEqual(subscriberA.RecivedMessages.Count, 1);
            Assert.AreEqual(subscriberB.RecivedMessages.Count, 2);
        }
    }
}
