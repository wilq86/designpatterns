﻿using DesignPatterns.Function.ChainOfResponsibilities;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Net;
using System.Text;

namespace DesignPatterns.Tests.Function
{
    [TestFixture]
    public class ChainOfResponsibilityTest
    {
        [Test]
        public void shouldTest()
        {
            IHandler handler = new IntHandler()
            {
                Next = new DoubleHandler()
            };

            Assert.AreEqual("10", handler.HandleRequest(10));
            Assert.AreEqual("10", handler.HandleRequest(10.0d));
        }
    }
}
