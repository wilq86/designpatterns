﻿using DesignPatterns.Function.TemplateMethods;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace DesignPatterns.Tests.Function.TemplateMethods
{

    [TestFixture]
    public class TemplateMethodTest
    {
        [Test]
        public void shouldTestTemplateMethod()
        {
            SumPositiveNumbersFormula sumFormula1 = new SumPositiveNumbersFormula(new List<double>() { 1, 2, 3, 4, 5});
            Assert.AreEqual(sumFormula1.Run(), 15 );

            SumPositiveNumbersFormula sumFormula2 = new SumPositiveNumbersFormula(new List<double>() { -1, 1, 2, 3, 4, 5});
            Assert.AreEqual(sumFormula2.Run(), null );

            MaxFormula maxFormula1 = new MaxFormula(new List<double>() { -1, 1, 2, 3, 4, 5});
            Assert.AreEqual(maxFormula1.Run(), 5);

            MaxFormula maxFormula2 = new MaxFormula(new List<double>());
            Assert.AreEqual(maxFormula2.Run(), null );
        }
    }
}
