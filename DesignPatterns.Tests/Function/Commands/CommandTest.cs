﻿using DesignPatterns.Function.Commands;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace DesignPatterns.Tests.Function.Commands
{
    [TestFixture]
    public class CommandTest
    {
        [Test]
        public void shouldTestCommand()
        {
            AddCommand addCommand = new AddCommand(15);
            MultipleCommand multipleCommand = new MultipleCommand(15);

            ValueItem value = new ValueItem(10);

            Assert.AreEqual(multipleCommand.Do(value).Value, 150);
            Assert.AreEqual(addCommand.Do(value).Value, 165);
            Assert.AreEqual(addCommand.Undo(value).Value, 150);
            Assert.AreEqual(multipleCommand.Undo(value).Value, 10);
        }
    }
}
