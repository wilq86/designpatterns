﻿using DesignPatterns.Function.Iterator;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace DesignPatterns.Tests.Function.Iterators
{
    [TestFixture]
    public class FibonaciIteratorTest
    {
        [Test]
        public void shouldGenerateFibonaciSeries()
        {
            var iterator = new FibonaciIterator();

            foreach (var item in iterator)
            {
                if (item > 100)
                {
                    break;
                }
            }

            var enumerator = iterator.GetEnumerator();

            Assert.AreEqual(enumerator.MoveNext(), true);
            Assert.AreEqual(enumerator.Current, 0);

            Assert.AreEqual(enumerator.MoveNext(), true);
            Assert.AreEqual(enumerator.Current, 1);

            Assert.AreEqual(enumerator.MoveNext(), true);
            Assert.AreEqual(enumerator.Current, 1);

            Assert.AreEqual(enumerator.MoveNext(), true);
            Assert.AreEqual(enumerator.Current, 2);

            Assert.AreEqual(enumerator.MoveNext(), true);
            Assert.AreEqual(enumerator.Current, 3);

            Assert.AreEqual(enumerator.MoveNext(), true);
            Assert.AreEqual(enumerator.Current, 5);

            Assert.AreEqual(enumerator.MoveNext(), true);
            Assert.AreEqual(enumerator.Current, 8);

            Assert.AreEqual(enumerator.MoveNext(), true);
            Assert.AreEqual(enumerator.Current, 13);

            Assert.AreEqual(enumerator.MoveNext(), true);
            Assert.AreEqual(enumerator.Current, 21);

            Assert.AreEqual(enumerator.MoveNext(), true);
            Assert.AreEqual(enumerator.Current, 34);

            Assert.AreEqual(enumerator.MoveNext(), true);
            Assert.AreEqual(enumerator.Current, 55);

            Assert.AreEqual(enumerator.MoveNext(), true);
            Assert.AreEqual(enumerator.Current, 89);

            Assert.AreEqual(enumerator.MoveNext(), true);
            Assert.AreEqual(enumerator.Current, 144);
        }
    }
}
