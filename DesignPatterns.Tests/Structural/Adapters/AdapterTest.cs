﻿using DesignPatterns.Structural.Adapters;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace DesignPatterns.Tests.Structural.Adapters
{
    [TestFixture]
    public class AdapterTest
    {
        [Test]
        public void shouldTestAdapter()
        {
            Adaptee adaptee = new Adaptee();
            ITarget target = new Adapter(adaptee);

            Assert.AreEqual(adaptee.MethodToAdaptee(), target.TargetMethod());
        }
    }
}
