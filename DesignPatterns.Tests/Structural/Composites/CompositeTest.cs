﻿using DesignPatterns.Structural.Composites;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace DesignPatterns.Tests.Structural.Composites
{
    [TestFixture]
    public class CompositeTest
    {
        [Test]
        public void shouldTestComposite()
        {
            BigBox bigBox = new BigBox();
            bigBox.Add(new ComputerCase());

            Assert.AreEqual(bigBox.CalculateCost(), 110.0d);
        }
    }
}
