﻿using DesignPatterns.Structural.Flyweights;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace DesignPatterns.Tests.Structural.Flyweights
{
    [TestFixture]
    public class FlyweightTest
    {

        [Test]
        public void shouldTestFlywight()
        {
            FontFactory fontFactory = new FontFactory();

            Font font12 = fontFactory.CreateFont(12, true, "Times New Roman");
            Font font14 = fontFactory.CreateFont(14, true, "Times New Roman");
            Font font16 = fontFactory.CreateFont(16, true, "Times New Roman");
            Font font12_2 = fontFactory.CreateFont(12, true, "Times New Roman");
            Font font14_2 = fontFactory.CreateFont(14, true, "Times New Roman");

            Assert.IsTrue(Object.ReferenceEquals(font12, font12_2));
            Assert.IsTrue(Object.ReferenceEquals(font14, font14_2));

        }
    }
}
