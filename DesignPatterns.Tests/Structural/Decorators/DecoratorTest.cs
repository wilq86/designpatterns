﻿using DesignPatterns.Structural.Decorators;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace DesignPatterns.Tests.Structural.Decorators
{
    [TestFixture]
    public class DecoratorTest
    {
        [Test]
        public void Test()
        {
            FileOperation fileOperation = new FileOperation();
            byte[] data = new byte[] { 0x00, 0x01, 0x02, 0x03, 0x04 };

            fileOperation.Save("test", data);
            fileOperation.Read("test");

            Assert.That(fileOperation.Read("test"), Is.EqualTo(data));


            fileOperation = new CompressFileOperation();

            fileOperation.Save("test", data);
            fileOperation.Read("test");

            Assert.That(fileOperation.Read("test"), Is.EqualTo(data));


            fileOperation = new EncryptFileOperation();

            fileOperation.Save("test", data);
            fileOperation.Read("test");

            Assert.That(fileOperation.Read("test"), Is.EqualTo(data));
        }
    }
}
