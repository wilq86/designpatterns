﻿using DesignPatterns.Structural.Proxies;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Security.Cryptography.X509Certificates;
using System.Text;

namespace DesignPatterns.Tests.Structural.Proxies
{
    [TestFixture]
    public class ProxyTest
    {
        [Test]
        public void shouldTest()
        {
            SendEmail realSubject = new SendEmail();

            Proxy proxy = new Proxy(realSubject);
            Assert.Throws<Exception>( () => proxy.Send() );
        }
    }
}
